angular.module('gotoPiApp',['ui.router','ngResource','gotoPiApp.controllers','gotoPiApp.services']);

angular.module('gotoPiApp').config(function($stateProvider,$httpProvider){
    $stateProvider.state('home',{
        url:'/home',
        templateUrl:'partials/pidata.html',
        controller:'HomeController'
    });
}).run(function($state){
   $state.go('home');
});
