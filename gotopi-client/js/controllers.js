angular.module('gotoPiApp.controllers',[]).controller('HomeController',function($scope,$state,$interval,popupService,$window,DataService){

    $scope.reload = function () {
        DataService.get().$promise.then(function(result){
            $scope.dataJson = result;

            var isMoveAlert = document.getElementById("isMoveAlert");
            var isLightAlert = document.getElementById("isLightAlert");

            if (result.isMove == true){
                isMoveAlert.className = "alert alert-danger";
                isMoveAlert.innerHTML = "Ruch!";
            }
            else if (result.isMove == false) {
                isMoveAlert.className = "alert alert-success";
                isMoveAlert.innerHTML = "Brak ruchu!";
            }

            if (result.isLight == true){
                isLightAlert.className = "alert alert-danger";
                isLightAlert.innerHTML = "Zapalone światło!";
            }
            else if (result.isMove == false) {
                isLightAlert.className = "alert alert-success";
                isLightAlert.innerHTML = "Ciemno!";
            }
        });
    };
    $scope.reload();
    $interval($scope.reload, 5000);
});
