angular.module('gotoPiApp.services',[]).factory('DataService',function($resource){
    return $resource('/gotopi/getData/', {}, {});
}).service('popupService',function($window){
    this.showPopup=function(message){
        return $window.confirm(message);
    }
});
