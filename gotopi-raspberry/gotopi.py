#!/usr/bin/python
import smbus
import RPi.GPIO as GPIO
import time
import requests
import json
import arrow

URL = 'http://192.166.219.240:8080/gotopi/saveData'
# inicjowanie czujnika ruchu
GPIO.setmode(GPIO.BOARD)
pir = 11
GPIO.setup(pir, GPIO.IN)
time.sleep(2)
print "Czujnik zainicjowany"

# inicjowanie czujnika swiatla
# Default device I2C address
DEVICE = 0x23
ONE_TIME_HIGH_RES_MODE_1 = 0x20
bus = smbus.SMBus(1)

# konwertowanie na floata
def convertToNumber(data):
    return ((data[1] + (256 * data[0])) / 1.2)

def readLight(addr=DEVICE):
    data = bus.read_i2c_block_data(addr,ONE_TIME_HIGH_RES_MODE_1)
    return convertToNumber(data)

while True:
    isLight = False
    isMove = False

    print "Swiatlo : " + str(readLight()) + " lx"
    if readLight() > 50:
        isLight = True
    if GPIO.input(pir):
        isMove = True

    dt = arrow.now().format('YYYY-MM-DDTHH:mm:ssZ')
    parameters = json.dumps({'isMove': isMove, 'isLight': isLight, 'dateTime': dt})
    print parameters
    headers = {}
    headers["Content-Type"] = "application/json"

    try:
        r = requests.put(URL, data=parameters, headers=headers)
        print r.text
    except requests.exceptions.RequestException as e:
        print e

    print "------------------------------------------------------------------"

    time.sleep(2)
