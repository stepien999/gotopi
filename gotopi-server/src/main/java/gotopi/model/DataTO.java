package gotopi.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class DataTO {
    private Boolean isMove;
    private Boolean isLight;
    private Date dateTime;
}
