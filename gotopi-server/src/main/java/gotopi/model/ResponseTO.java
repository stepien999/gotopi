package gotopi.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ResponseTO {
    private String message;
    private String exception;

    public ResponseTO(ResponseEnum message) {
        this.message = message.toString();
    }

    public ResponseTO(ResponseEnum message, Exception e) {
        this.message = message.toString();
        this.setException(e.getMessage());
    }
}
