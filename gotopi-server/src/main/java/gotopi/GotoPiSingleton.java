package gotopi;

import gotopi.model.DataTO;

public class GotoPiSingleton {
    private static GotoPiSingleton ourInstance = new GotoPiSingleton();
    private DataTO data;

    public static GotoPiSingleton getInstance() {
        return ourInstance;
    }

    private GotoPiSingleton() {
        data = new DataTO(false, false, null);
    }

    public DataTO getData() {
        return data;
    }

    public void setData(DataTO data) {
        this.data = data;
    }
}
