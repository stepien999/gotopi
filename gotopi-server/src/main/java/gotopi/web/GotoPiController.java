package gotopi.web;

import gotopi.GotoPiSingleton;
import gotopi.model.DataTO;
import gotopi.model.ResponseEnum;
import gotopi.model.ResponseTO;
import gotopi.service.GotoPiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class GotoPiController {

    @Autowired
    private GotoPiService gotoPiService;

    @GetMapping("/")
    @ResponseBody
    public String helloWorld() {
        return "test";
    }

    @SuppressWarnings({
            "rawtypes"
    })
    @RequestMapping(
            value = "/saveData",
            method = RequestMethod.PUT,
            produces = "application/json; charset=UTF-8",
            consumes = "application/json; charset=UTF-8")
    @ResponseBody
    public ResponseTO saveData(@RequestBody DataTO dataTO) {
        try {
            gotoPiService.savaData(dataTO);
            return new ResponseTO(ResponseEnum.SUCCESS);
        } catch (Exception e) {
            return new ResponseTO(ResponseEnum.EXCEPTION, e);
        }
    }

    @GetMapping("/getData")
    @ResponseBody
    public DataTO getData() {
        return GotoPiSingleton.getInstance().getData();
    }
}
