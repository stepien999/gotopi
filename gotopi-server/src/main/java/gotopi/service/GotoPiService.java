package gotopi.service;

import gotopi.GotoPiSingleton;
import gotopi.model.DataTO;
import org.springframework.stereotype.Component;

@Component
public class GotoPiService {

    public void savaData(DataTO dataTO) {
       GotoPiSingleton.getInstance().setData(dataTO);
    }
}
